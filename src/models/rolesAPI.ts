export class ManageRoleAPI {
    roleDescription: string;
    roleId: number;
    roleName: string;
}

export interface Content {
    description: string;
    roleId: number;
    roleName: string;
}

export interface RoleResponseAPI {
    content: Content;
    message: string;
}



