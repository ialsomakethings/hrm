export class Search {
    searchString: string;
}

export interface Authority {
    authority: string;
}

export interface Role {
    description: string;
    roleId: number;
    roleName: string;
}

export interface AccountDto {
    accountId: number;
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    authorities: Authority[];
    credentialsNonExpired: boolean;
    enabled: boolean;
    roles: Role[];
    status: number;
    username: string;
}

export interface Position {
    description: string;
    positionId: number;
    positionName: string;
}

export interface Content {
    accountDto: AccountDto;
    address: string;
    countryCode: string;
    dob: Date;
    email: string;
    firstName: string;
    idCardNo: string;
    lastName: string;
    phoneNumber: string;
    position: Position;
    status: string;
}

export interface SearchResponse {
    content: Content[];
    message: string;
}
