export class MenuItem {
    ID: number;
    parentID: number;
    title: string;
    class: string;
    href: string;
    child: MenuItem[];
}
