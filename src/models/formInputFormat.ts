export class FormInput {
    Label: string;
    Type: string;
    Class: string;
    PlaceHolder: string;
}
