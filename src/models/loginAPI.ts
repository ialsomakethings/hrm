export class LoginRequest {
    username: string;
    password: string;
}

export interface Content {
    token: string;
    username: string;
    tokenType: string;
}

export interface LoginResponseAPI {
    message: string;
    content: Content;
}
