
// Send API
export class SearchEmployeeAPI {
    positionId: number;
    username: string;
}
export class ChangeEmployeeStatusAPI {
    employeeId: number;
    positionId: number;
    statusId: number;
}

export class ManageEmployeeAPI {
    address: string;
    countryCode: string;
    dob: string;
    employeeId: number;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    positionId: number;
    salary: number;

}

// Response

export interface EmployeeResponseAPI {
    content: Content[];
    message: string;
}
export interface Authority {
    authority: string;
}

export interface Role {
    description: string;
    roleId: number;
    roleName: string;
}

export interface AccountDto {
    accountId: number;
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    authorities: Authority[];
    credentialsNonExpired: boolean;
    enabled: boolean;
    roles: Role[];
    status: number;
    username: string;
}

export interface Position {
    description: string;
    positionId: number;
    positionName: string;
}

export interface Content {
    accountDto: AccountDto;
    address: string;
    countryCode: string;
    dob: Date;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    position: Position;
    salary: number;
    status: string;
}



