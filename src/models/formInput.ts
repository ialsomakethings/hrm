export class Account {
    accountId: number;
    accountNonExpired: boolean;

}


export class Employee {
   address: string;
   countryCode: string;
   dob: string;
   email: string;
   employeeId: number;
   firstName: string;
   idCardNo: string;
   lastName: string;
   phoneNumber: string;
   positionId: number;
   salary: number;
}

export class Position {
    DESCRIPTION: string;
    POSITION_ID: number;
    POSITION_NAME: string;
}
