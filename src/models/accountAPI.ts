
export class ChangePasswordRequest {
    newPassword: string;
    oldPassword: string;
    username: string;
}

export interface Authority {
    authority: string;
}

export interface Role {
    description: string;
    roleId: number;
    roleName: string;
}

export interface Content {
    accountId: number;
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    authorities: Authority[];
    credentialsNonExpired: boolean;
    enabled: boolean;
    roles: Role[];
    status: number;
    username: string;
}

export interface AccountResponseAPI {
    content: Content;
    message: string;
}


export class ChangeAccountRolesAPI {
    accountId: number;
    roleId: number[];
}

export class ChangeAccountStatusAPI {
    accountId: number;
    statusId: number;
}
