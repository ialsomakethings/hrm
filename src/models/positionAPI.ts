export class ManagePositionAPI {
    positionDescription: string;
    positionName: string;
    positonId: number;
}

export interface Content {
    description: string;
    positionId: number;
    positionName: string;
}

export interface PositionResponseAPI {
    content: Content;
    message: string;
}


