import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutComponent } from '../cms/layout/layout.component';
import { Row } from 'src/models/searchRows';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  text: string;
  title = 'CMS HRM';
  disabled = true;
  searchList = false;
  rowList: Row[];
  constructor(private router: Router) {

    this.rowList = [{
      Name: 'Phạm Hoàng Dũng',
      DOB: '1997',
      Age: 22,
      Position: 'Dev',
      PhoneNumber: '0332570529'
    }, {
      Name: 'Phạm Hoàng Dũng',
      DOB: '1997',
      Age: 22,
      Position: 'Dev',
      PhoneNumber: '0332570529'
    }, {
      Name: 'Phạm Hoàng Dũng',
      DOB: '1997',
      Age: 22,
      Position: 'Dev',
      PhoneNumber: '0332570529'
    }, {
      Name: 'Phạm Hoàng Dũng',
      DOB: '1997',
      Age: 22,
      Position: 'Dev',
      PhoneNumber: '0332570529'
    }, {
      Name: 'Phạm Hoàng Dũng',
      DOB: '1997',
      Age: 22,
      Position: 'Dev',
      PhoneNumber: '0332570529'
    }, {
      Name: 'Phạm Hoàng Dũng',
      DOB: '1997',
      Age: 22,
      Position: 'Dev',
      PhoneNumber: '0332570529'
    }];
  }

  ngOnInit() {
  }

  navigateCMS() {
    this.router.navigate(['cms']);
  }
  toggleDisabled() {
    this.disabled = !this.disabled;
  }

  Search($element) {
    this.searchList = true;
    $element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }


}
