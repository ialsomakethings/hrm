import { Component, OnInit, Input } from '@angular/core';
import { Row } from 'src/models/searchRows';

@Component({
  selector: 'app-search-row',
  templateUrl: './search-row.component.html',
  styleUrls: ['./search-row.component.css']
})
export class SearchRowComponent implements OnInit {
  @Input() row: Row;
  constructor() { }

  ngOnInit() {
  }

}
