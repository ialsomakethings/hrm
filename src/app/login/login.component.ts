import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginRequest, LoginResponseAPI } from 'src/models/loginAPI';
import { APIService } from '../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BuiltinVar, tokenName } from '@angular/compiler';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  success = false;

  requestLogin: LoginRequest = new LoginRequest();

  loginResponse: LoginResponseAPI;

  constructor(private formBuilder: FormBuilder, private loginServices: APIService, private route: ActivatedRoute, private router: Router) {

    this.success = false;
    this.loginForm = this.formBuilder.group({
      username: [this.requestLogin.username, Validators.required],
      password: [this.requestLogin.password, Validators.required]
    });
  }

  onSubmit() {

    this.submitted = true;

    if (this.loginForm.invalid) {
      return false;
    }
    if (this.success) {
      return false;
    }
    return true;

  }
  sendLogin() {
    if (!this.onSubmit()) { return; }
    // console.log(this.loginServices.loginStatus);
    // console.log(this.success);
    let token: string;
    this.requestLogin.username = this.loginForm.controls.username.value;
    this.requestLogin.password = this.loginForm.controls.password.value;

    // this.loginServices.loginStatus = true;
    // this.router.navigate(['/cms']);
    this.loginServices.submitLogin(this.requestLogin).subscribe(data => {
      try {
        this.loginResponse = data as LoginResponseAPI;
        token = `${this.loginResponse.content.tokenType} ${this.loginResponse.content.token}`;
        sessionStorage.setItem(btoa('token'), btoa(token));
        console.log(sessionStorage.getItem('abc'));
        console.log(token);
        this.loginServices.loginStatus = true;
        this.router.navigate(['/cms']);
      } catch {
        console.log(`error`);
      }
    });

  }
  ngOnInit() {


  }

}
