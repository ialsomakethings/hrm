import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';


import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { InputMaskModule } from 'primeng/inputmask';
import { MatIconModule } from '@angular/material';

import { LocationStrategy, PathLocationStrategy } from '@angular/common';

import { AppRoutingModule, RoutingComponents } from './app-routing.module';
import { FooterComponent } from './CMS/shared/footer/footer.component';
import { SidebarComponent } from './CMS/shared/sidebar/sidebar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { EmployeeComponent } from './CMS/page/employee/employee.component';

import { CalendarModule } from 'primeng/calendar';
import { FormInputComponent } from './cms/page/form-input/form-input.component';
import { SidebarItemComponent } from './cms/shared/sidebar-item/sidebar-item.component';
import { EmployeeInputComponent } from './cms/page/form-input/employee-input/employee-input.component';
import { AccountInputComponent } from './cms/page/form-input/account-input/account-input.component';
import { PositionInputComponent } from './cms/page/form-input/position-input/position-input.component';
import { RolesInputComponent } from './cms/page/form-input/roles-input/roles-input.component';
import { EmployeeListComponent } from './cms/page/list/employee-list/employee-list.component';
import { AccountListComponent } from './cms/page/list/account-list/account-list.component';
import { RoleListComponent } from './cms/page/list/role-list/role-list.component';
import { PositionListComponent } from './cms/page/list/position-list/position-list.component';
import { SearchRowComponent } from './landing-page/search-row/search-row.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingPageComponent,
    RoutingComponents,
    FooterComponent,
    SidebarComponent,
    PageNotFoundComponent,
    EmployeeComponent,
    FormInputComponent,
    SidebarItemComponent,
    EmployeeInputComponent,
    AccountInputComponent,
    PositionInputComponent,
    RolesInputComponent,
    EmployeeListComponent,
    AccountListComponent,
    RoleListComponent,
    PositionListComponent,
    SearchRowComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    InputTextModule,
    ButtonModule,
    NgbModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatIconModule,
    InputMaskModule,
    CalendarModule
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: PathLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
