import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './cms/layout/layout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { EmployeeComponent } from './CMS/page/employee/employee.component';
import { AuthGuard } from './CMS/authenticate/auth.guard';
import { FormInputComponent } from './cms/page/form-input/form-input.component';
import { EmployeeInputComponent } from './cms/page/form-input/employee-input/employee-input.component';
import { AccountListComponent } from './cms/page/list/account-list/account-list.component';

const AppRoutes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'cms', component: LayoutComponent,
    children: [
      {
        path: '',
        component: FormInputComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'employee',
        component: EmployeeInputComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'form',
        component: FormInputComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'account',
        component: AccountListComponent,
        canActivate: [AuthGuard],
      },
      // path
      {
        path: '**',
        redirectTo: '/cms',
        canActivate: [AuthGuard]
      }
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(AppRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
export const RoutingComponents = [LandingPageComponent, LoginComponent, LayoutComponent];
