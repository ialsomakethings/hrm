import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { APIService } from '../../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  constructor(public auth: APIService, public router: Router) { }
  canActivate(): boolean {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate(['/login']);
      console.log(`Log: ${false}`);
      return false;
    }
    return true;
  }
}
