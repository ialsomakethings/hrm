import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesInputComponent } from './roles-input.component';

describe('RolesInputComponent', () => {
  let component: RolesInputComponent;
  let fixture: ComponentFixture<RolesInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
