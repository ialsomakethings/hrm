import { Component, OnInit } from '@angular/core';
import { FormInput } from 'src/models/formInputFormat';
import * as $ from 'jquery';
@Component({
  selector: 'app-employee-input',
  templateUrl: './employee-input.component.html',
  styleUrls: ['./employee-input.component.css']
})
export class EmployeeInputComponent implements OnInit {
  inputList: FormInput[];
  title = 'Employee';
  constructor() {
    this.inputList = [
      {
        Label: 'First Name',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ''
      }, {
        Label: 'Last Name',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ' '
      }, {
        Label: 'Date Of Birth',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ' '
      }, {
        Label: 'Email',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ' '
      }, {
        Label: 'CIF',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ' '
      }, {
        Label: 'Employee ID',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ' '
      }, {
        Label: 'Position ID',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ' '
      }, {
        Label: 'Salary',
        Class: 'form-control',
        Type: '',
        PlaceHolder: ' '
      }];
  }

  ngOnInit() {
  }
  $function() {
    $('#datemask').inputmask('dd/mm/yyyy', { placeholder: 'dd/mm/yyyy' });
  }
}
