import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem } from '../../../../models/menu';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  abc = 'fa fa-share';
  inputList: MenuItem[];
  menuList: MenuItem[];

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.inputList = [
      {
        ID: 1,
        parentID: 0,
        title: 'Account Manager',
        class: 'fa fa-user',
        href: '',
        child: [{
          ID: 2,
          parentID: 1,
          title: 'Add Account',
          class: 'fa fa-circle-o',
          href: 'form',
          child: []
        },
        {
          ID: 3,
          parentID: 1,
          title: 'Account List',
          class: 'fa fa-circle-o',
          href: 'account',
          child: []
        }]
      },
      {
        ID: 4,
        parentID: 0,
        title: 'HR Manager',
        class: 'fa fa-address-book',
        href: '',
        child: [{
          ID: 5,
          parentID: 4,
          title: 'HR Child 1',
          class: 'fa fa-circle-o',
          href: '',
          child: []
        },
        {
          ID: 6,
          parentID: 4,
          title: 'HR Child 2',
          class: 'fa fa-circle-o',
          href: '',
          child: []
        }]
      },
      {
        ID: 7,
        parentID: 0,
        title: 'Category',
        class: 'fa fa-phonebook',
        href: '',
        child: [{
          ID: 8,
          parentID: 7,
          title: 'Role',
          class: 'fa fa-circle-o',
          href: '',
          child: []
        }, {
          ID: 8,
          parentID: 7,
          title: 'Menu',
          class: 'fa fa-circle-o',
          href: '',
          child: []
        }, {
          ID: 8,
          parentID: 7,
          title: 'Permission',
          class: 'fa fa-circle-o',
          href: '',
          child: []
        }]
      }
    ];
    this.menuList = this.inputList;
  }
  showRoles() {
    // this.router.navigate(['employee'], { relativeTo: this.route });
  }
  setClasses(icon) {
    return icon;
  }
  navigateTo(address) {
    this.router.navigate([address], { relativeTo: this.route });
  }
}
