import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from 'src/models/menu';

@Component({
  selector: 'app-sidebar-item',
  templateUrl: './sidebar-item.component.html',
  styleUrls: ['./sidebar-item.component.css']
})
export class SidebarItemComponent implements OnInit {
  @Input() menuItem: MenuItem;
  classes: string;
  constructor() { }

  ngOnInit() {
    this.classes = this.menuItem.class;
  }
  setClasses() {

  }
}
