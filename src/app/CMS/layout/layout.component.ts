import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authenticate/auth.service';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  // public myClass:string = 'hold-transition skin-blue sidebar-mini';
  public myClass = 'skin-blue sidebar-mini';
  public menu = false;
  show = false;
  constructor(private authService: AuthService) { this.show = false; }

  toggleSidebar() {
    this.menu = !this.menu;
    if (this.menu) {
      this.myClass = 'skin-blue sidebar-mini sidebar-collapse';
    } else {
      this.myClass = 'skin-blue sidebar-mini';
    }
  }
  showMenu() {
    this.show = !this.show;
  }
  signOut() {
    this.authService.auth.LoggedOut();
  }
  ngOnInit() {
    this.show = false;
  }

}
