import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { LoginRequest } from 'src/models/loginAPI';
import { Observable, of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { MessageService } from './message.service';
import { Router } from '@angular/router';
import { ChangePasswordRequest, ChangeAccountRolesAPI, ChangeAccountStatusAPI } from 'src/models/accountAPI';
import { ManageEmployeeAPI } from 'src/models/employeeAPI';
import { ManagePositionAPI } from 'src/models/positionAPI';
import { ManageRoleAPI } from 'src/models/rolesAPI';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json;charset=UTF-8',
    Accept: '*/*',
    Authorization: atob(sessionStorage.getItem(btoa('token')))
  })
};

@Injectable({
  providedIn: 'root'
})

export class APIService {
  apiUrl = 'http://192.168.100.12:8088/api';
  loginStatus = false;
  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private router: Router) { }



  isLoggedIn() {
    if (null !== sessionStorage.getItem(btoa('token'))) { return true; }
    return false;
  }
  LoggedOut() {
    this.loginStatus = false;
    sessionStorage.removeItem(btoa('token'));
    // tslint:disable-next-line:no-unused-expression
    this.router.navigate['/login'];
  }


  submitLogin(payload: LoginRequest): Observable<any> {
    return this.http.post(`${this.apiUrl}/auth/login`, payload, httpOptions).pipe(catchError(this.handleError<any>('submitLogin')));
  }

  changePassword(payload: ChangePasswordRequest): Observable<any> {
    return this.http.post(`${this.apiUrl}/account/change-password`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('changePassword')));
  }

  getProfile(): Observable<any> {
    return this.http.get(`${this.apiUrl}/account/get-profile`, httpOptions)
      .pipe(catchError(this.handleError<any>('getProfile')));
  }

  getAccount(): Observable<any> {
    return this.http.get(`${this.apiUrl}/account/getaccount`, httpOptions)
      .pipe(catchError(this.handleError<any>('getAccount')));
  }

  searchEmployee(searchString): Observable<any> {
    return this.http.get(`${this.apiUrl}/emp/search-employee?ss=${searchString}`, httpOptions)
      .pipe(catchError(this.handleError<any>('searchEmployee')));
  }

  addEmployeeApi(payload: ManageEmployeeAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/manage/add-employee`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('addEmployeeApi')));
  }

  updateEmployee(payload: ManageEmployeeAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/mange/update-employee-information`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('updateEmployee')));
  }

  changeAccountRole(payload: ChangeAccountRolesAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/admin/change-role`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('changeAccountRole')));
  }

  changeAccountStatus(payload: ChangeAccountStatusAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/admin/change-status`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('changeAccountStatus')));
  }

  addNewPosition(payload: ManagePositionAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/admin/add-new-position`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('addPosition')));
  }

  updatePosition(payload: ManagePositionAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/admin/update-position`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('updatePosition')));
  }

  addNewRole(payload: ManageRoleAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/admin/new-role`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('addNewRole')));
  }

  updateRole(payload: ManageRoleAPI): Observable<any> {
    return this.http.post(`${this.apiUrl}/admin/new-role`, payload, httpOptions)
      .pipe(catchError(this.handleError<any>('updateRole')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // console.log(`failed:${error.status}, ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
